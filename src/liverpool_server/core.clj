(ns liverpool-server.core
  ; (:require [org.httpkit.server :as server]
  ;           [compojure.core :as core]
  ;           [compojure.route :as route]
  ;           [ring.middleware.defaults :as defaults]
  ;           [clojure.pprint :as pp]
  ;           [clojure.string :as str]
  ;           [clojure.data.json :as json])
  (:gen-class))

(def start-time (System/nanoTime))

(defrecord Card [deck suit rank])
(defmethod print-method Card
  [card writer]
  (print-simple (str (.deck card) ":" (.rank card) (.suit card)) writer))

(defn generate-suits
  "Lazily generate suits for a rank"
  [deck rank]
  (list
   (Card. deck :c rank)
   (Card. deck :d rank)
   (Card. deck :s rank)
   (Card. deck :h rank)))
(defn generate-deck
  "Lazily generate a deck"
  [deck-id]
  (let [ranks (range 1 14)]
    (concat
     (mapcat (partial generate-suits deck-id) ranks)
     (list (Card. deck-id :j 1)
           (Card. deck-id :j 2)))))

(def original-deck
  "The original Liverpool deck"
  (let [deck-a (generate-deck "A")
        deck-b (generate-deck "B")]
    (concat deck-a deck-b)))

;; Set up state
(def hands
  "The hands in the current game"
  (atom []))
(def draw-pile (atom (list)))
(def live-discard (atom nil))

;; Set up deck functions
(defn generate-hands
  [num-hands]
  (into [] (map (fn [_] (atom [])) (range num-hands))))
(defn draw-pile-insert-bottom_
  "Add new cards to the bottom of the draw pile"
  [current-draw-pile new-cards]
  (concat current-draw-pile (shuffle new-cards)))
(defn add-cards!
  "Shuffle new cards and add them to the bottom of the draw pile"
  [new-cards]
  (swap! draw-pile draw-pile-insert-bottom_ new-cards))
(defn is-valid-player?
  "Check if a player is valid in the current game, throwing an exception if not"
  [player-idx]
  (if (or (< player-idx 0) (>= player-idx (count @hands)))
    (do (throw (IndexOutOfBoundsException. (format "Player %s does not exist in this game." player-idx))) false)
    true))
(defn add-to-hand_!
  "Add a card to a player's hand"
  [player-idx card]
  (swap! (nth @hands player-idx) conj card))
(defn discard!
  "Sets the active discard"
  [card]
  (swap! live-discard (fn [old new] new) card))
(defn draw!
  "Draw a card into a player's hand, either from the draw pile or active discard"
  [player-idx draw-discard]
  (if (is-valid-player? player-idx)
    (if (and draw-discard (some? @live-discard)) ; only allow drawing the discard if it's live
      (let [draw-card @live-discard]
        (discard! nil)
        draw-card)
      (do (if (< (count @draw-pile) 1) (add-cards! (generate-deck "C")) nil) ;; TODO: update the new deck identifier
          (let [draw-card (first @draw-pile)]
            (swap! draw-pile rest)
            (add-to-hand_! player-idx draw-card)
            draw-card)))
    nil))
(defn deal! []
  (dotimes [hand (count @hands)]
    (dotimes [card-num 11]
      (draw! hand false))))
(defn new-game!
  "Create a new game.  Initialize the hands and deck."
  [num-players]
  (discard! nil)
  (swap! hands (fn [_] (generate-hands num-players)))
  (swap! draw-pile (fn [_] (list)))
  (add-cards! original-deck)
  (deal!))

;; Validate contracts
(defn joker?
  "Whether or not a card is a joker"
  [card] (= (.suit card) :j))
(defn get-suit [card] (.suit card))
(defn get-rank [card] (.rank card))
(defn validate-set
  "Check whether or not the cards form a set"
  [cards]
  (let [num-jokers (count (filter joker? cards))
        nonjokers (filter (complement joker?) cards)
        threshold (max (- 3 num-jokers) 1)] ;; Make sure there is 1+ non-joker and 3+ total cards
    (and (>= (count nonjokers) threshold) (apply = (map get-rank nonjokers)))))
(defn validate-run
  "Check whether or not the cards form a run"
  [cards]
  (let [num-cards (count cards)
        suits (->> cards (filter (complement joker?)) (map get-suit) distinct)
        suit (first suits) ;; Assume that there's only one suit
        ;; Find the first non-joker and compute the offset between its index and rank
        idx-offset (first (filter #(> % -1) (map-indexed (fn [idx card] (if (joker? card) -1 (- (.rank card) idx))) cards)))
        ;; Collect intended cards & values with their respective indices
        imputed-cards (map-indexed (fn [idx card]
                                     (let [deck (.deck card)]
                                       (if (joker? card)
                                         ;; Replace jokers with intended values
                                         [idx (Card. deck suit (+ idx idx-offset))]
                                         (if (and (= (.rank card) 1) (>= idx (/ num-cards 2)))
                                           ;; Replace high aces with rank 14
                                           [idx (Card. deck suit 14)]
                                           ;; Leave everything else
                                           [idx card])))) cards)]
    (and
     ;; Must have at least 4 cards for a run
     (>= num-cards 4)
     ;; Can't use the ace twice
     (<= num-cards 13)
     ;; Must be same suit
     (= (count suits) 1)
     ;; Must have same offset between rank and index
     (apply = (map #(- (get-rank (last %)) (first %)) imputed-cards))
     ;; Must be valid ranks
     (>= (get-rank (last (first imputed-cards))) 1)
     (<= (get-rank (last (last imputed-cards))) 14))))

;; (println (/ (- (System/nanoTime) start-time) 1000000.0) "ms")

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (new-game! 3)
  (println @(nth @hands 0))
  (println @(nth @hands 1))
  (println @(nth @hands 2))
  (println @draw-pile)

  (draw! 0 false)
  (draw! 1 false)
  (draw! 2 false)
  (println @(nth @hands 0))
  (println @(nth @hands 1))
  (println @(nth @hands 2))
  (println @draw-pile))