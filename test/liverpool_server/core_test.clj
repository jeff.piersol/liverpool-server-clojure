(ns liverpool-server.core-test
  (:require [clojure.test :refer :all]
            [liverpool-server.core :as lp]))


(def valid-set [(lp/->Card "A" :c 1) (lp/->Card "B" :s 1) (lp/->Card "A" :h 1)])
(def valid-set-jokers [(lp/->Card "A" :j 1) (lp/->Card "A" :j 2) (lp/->Card "A" :h 1)])

(def valid-run1 [(lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :c 3) (lp/->Card "A" :c 4)])
(def valid-run2 [(lp/->Card "B" :c 11) (lp/->Card "A" :c 12) (lp/->Card "A" :c 13) (lp/->Card "A" :c 1)])
(def valid-run3 [(lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :j 2) (lp/->Card "A" :c 4)])
(def valid-run4 [(lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :j 2) (lp/->Card "A" :c 4) (lp/->Card "A" :c 5) (lp/->Card "B" :c 6) (lp/->Card "A" :j 1) (lp/->Card "A" :c 8) (lp/->Card "A" :c 9) (lp/->Card "A" :c 10) (lp/->Card "B" :c 11) (lp/->Card "A" :c 12) (lp/->Card "A" :c 13)])
(def invalid-run1 [(lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :s 3) (lp/->Card "A" :c 4)])
(def invalid-run2 [(lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :c 4) (lp/->Card "A" :c 5)])
(def invalid-run3 [(lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :c 4) (lp/->Card "A" :c 3)])
(def invalid-run4 [(lp/->Card "A" :c 12) (lp/->Card "A" :c 13) (lp/->Card "A" :c 1) (lp/->Card "A" :j 1)])
(def invalid-run5 [(lp/->Card "A" :j 1) (lp/->Card "A" :c 1) (lp/->Card "B" :c 2) (lp/->Card "A" :c 3)])
(def invalid-run6 [(lp/->Card "B" :c 2) (lp/->Card "A" :c 3) (lp/->Card "A" :c 4)])
(def invalid-run7 [(lp/->Card "A" :c 12) (lp/->Card "A" :c 13) (lp/->Card "A" :c 1) (lp/->Card "A" :c 1) (lp/->Card "A" :c 1) (lp/->Card "A" :j 1)])

(deftest scoring-tests
  (testing "Set validation"
    (is (lp/validate-set valid-set) "Normal set")
    (is (lp/validate-set valid-set-jokers) "Set with jokers"))
  (testing "Run validation"
    (is (lp/validate-run valid-run1) "Normal run")
    (is (lp/validate-run valid-run2) "Ace-high run")
    (is (lp/validate-run valid-run3) "Run with joker")
    (is (lp/validate-run valid-run4) "Big run")
    (is (not (lp/validate-run invalid-run1)) "Different suits")
    (is (not (lp/validate-run invalid-run2)) "Non-contiguous ranks")
    (is (not (lp/validate-run invalid-run3)) "Cards presented out of order")
    (is (not (lp/validate-run invalid-run4)) "Joker too high")
    (is (not (lp/validate-run invalid-run5)) "Joker too low")
    (is (not (lp/validate-run invalid-run6)) "Too few cards")
    (is (not (lp/validate-run invalid-run7)) "Invalid 7"))
  ;; (testing "Player size bounds"
  ;;   (is (thrown? Exception (lp/))))
  )
